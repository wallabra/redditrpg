import collections
import re
import typing
import uuid

from .canvas import CanvasPen, TextCanvas
from typing import Optional, Iterable, Any, Tuple, TypeVar, Type



ComponentType = TypeVar('TypeVar')


class Manager(object):
    def __init__(self, game, entities = None):
        self.game = game
        self.entities = collections.OrderedDict(entities or ())
        self.systems = []
        self.event_listeners = {}

    def __iter__(self):
        return iter(self.entities.values())

    def tick(self):
        for e in self.entities.values():
            for s in self.systems:
                s._tick(e)

    def render(self, canvas: TextCanvas):
        pen = CanvasPen(canvas)

        for e in self.entities.values():
            for s in self.systems:
                s._render(e, pen)

            p = canvas.pivot
            canvas.pivot = (0, 0)

            for s in self.systems:
                s._overlay(e, pen)

            canvas.pivot = p

    def add_system(self, s: Type['System']) -> None:
        self.systems.append(s(self))

    def remove_entity(self, e: 'Entity') -> None:
        del self.entities[e.id]

    def create_entity(self, components: Optional[Iterable[Tuple[str, Any]]] = (), identifier: Optional[str] = None) -> 'Entity':
        e = Entity(self, components, identifier)
        self.add_entity(e)
        
        return e

    def add_entity(self, e: 'Entity') -> None:
        self.entities[e.id] = e

    def listen(self, event_name):            
        def _decorator(self, func):
            if event_name not in self.event_listeners:
                self.event_listeners[event_name] = set()
            
            self.event_listeners[event_name] |= {func}
            return func

        return _decorator

    def add_listener(self, event_name, func):
        if event_name not in self.event_listeners:
            self.event_listeners[event_name] = set()

        self.event_listeners[event_name] |= {func}

    def emit(self, source, event_name, *args, **kwargs):
        if event_name in self.event_listeners:
            for func in self.event_listeners[event_name]:
                func(source, *args, **kwargs)

        check_name = '_'.join(event_name) if isinstance(event_name, (tuple, list)) else event_name

        for system in self.systems:
            system._on(check_name, source, *args, **kwargs)

    def __getitem__(self, entity_id: str) -> 'Entity':
        return self.entities[entity_id]


component_types = {}

def register_component(cotype):
    component_types[cotype.__name__] = cotype
    return cotype

@register_component
class Component(object):
    def __init__(self, entity: 'Entity', name: str, value: Optional[Any] = None):
        self.entity = entity
        self.name = name
        self.value = value
    
    def set(self, value):
        self.value = value
        self.entity.manager.emit(self.entity, ('change', self.name), self.value)


class Entity(object):
    def __init__(self, manager: Manager, components: Optional[Iterable[Tuple[str, Any]]] = None, identifier = None):
        self.manager = manager
        self.id = identifier or str(uuid.uuid4())
        self.components = [] # type: List[Component]
        self.comp_index = {} # type: Dict[str, Component]

        for c in components:
            self.add_component(*c)

    def __iter__(self):
        return (n.name for n in self.components)

    def remove(self):
        self.manager.remove_entity(self)

    def add_component(self, name: str, value: Optional[Any] = None, kind: ComponentType = Component) -> Component:
        if name in self.comp_index:
            c = self.comp_index[name]
            c.value = value

        else:
            c = kind(self, name, value)
            self.components.append(c)
            self.comp_index[c.name] = c

        return c

    def __getitem__(self, comp_name: str) -> Component:
        return self.comp_index[comp_name]

    def __setitem__(self, comp_name: str, value: Optional[Any] = None):
        if comp_name in self:
            c = self[comp_name]
            c.value = value

        else:
            self.add_component(comp_name, value)

    def __hasitem__(self, comp_name: str) -> bool:
        return comp_name in self.comp_index

    def __delitem__(self, comp_name: str):
        if comp_name not in self:
            return

        c = self.comp_index[comp_name]
        del self.comp_index[comp_name]

        while c in self.components:
            self.components.remove(c)

    def has(self, *comp_names: Iterable[str]) -> bool:
        return all(cn in self for cn in comp_names)

    def has_any(self, *comp_names: Iterable[str]) -> bool:
        return any(cn in self for cn in comp_names)


class System(object):
    listeners = []
    component_types = []

    @classmethod
    def component_check(cls, func):
        def _inner(source: Entity, *args, **kwargs):
            if not cls.component_types:
                return func(source, *args, components = None, **kwargs)

            else:
                components = {}

                for cotype in cls.component_types:
                    if cotype in source:
                        components[cotype] = source[cotype]

                    else:
                        return None

                return func(source, *args, components = components, **kwargs)

        return _inner

    def __init__(self, manager: Manager):
        self.manager = manager

        for event_name, func in self.listeners:
            manager.init_event(event_name)
            manager.add_listener(event_name, func)
    
    def _tick(self, entity: Entity, *args) -> None:
        self.component_check(self.tick)(entity, *args)

    def _render(self, entity: Entity, pen: CanvasPen, *args) -> None:
        self.component_check(self.render)(entity, pen, *args)

    def _overlay(self, entity: Entity, pen: CanvasPen, *args) -> None:
        self.component_check(self.overlay)(entity, pen, *args)

    def _on(self, event_name: str, entity: Entity, *args, **kwargs):
        if hasattr(self, 'on_' + event_name):
            return self.component_check(getattr(self, 'on_' + event_name))(entity, *args, **kwargs)

    def tick(self, entity: Entity, *args, components: typing.Dict[str, Component] = None) -> None:
        pass

    def render(self, entity: Entity, pen: CanvasPen, components: typing.Dict[str, Component] = None) -> None:
        pass

    def overlay(self, entity: Entity, pen: CanvasPen, components: typing.Dict[str, Component] = None) -> None:
        pass