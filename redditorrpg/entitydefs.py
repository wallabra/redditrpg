from .entity import Entity, Component, System, Manager, register_component
from .inventory import ItemContainer
from enum import Enum
from .canvas import CanvasPen

from typing import Optional, Any, Tuple


sys = []

def system_type(st):
    sys.append(st)
    return st



#=== Custom Component Types ===

@register_component
class InventoryComponent(Component, ItemContainer):
    def __init__(self, entity, name: str, value: Optional[Any] = ()):
        self.entity = entity
        self.name = name

        self.value = dict(value)
        self.items = self.value



#=== Entity Helper Methods ===

def force_move(ent: Entity, x: int, y: int):
    assert 'position' in ent

    pos = ent['position'].value

    pos[0] += x
    pos[1] += y


def move(manager: Manager, ent: Entity, x: int, y: int):
    force_move(ent, x, y)

    x1, y1 = ent['position'].value
    manager.emit(ent, 'move', (x, y))
    x2, y2 = ent['position'].value

    return x1 != x2 or y1 != y2


def get_name(ent: Entity):
    assert 'name' in ent

    return ent['name'].value


def in_geometry(entity: Entity, coord: Tuple[int, int]):
    if 'geometry' not in entity:
        return

    x, y = coord
    gtype = entity['geometry'].value

    if 'position' in entity:
        ex, ey = entity['position'].value

    else:
        ex = 0
        ey = 0

    if gtype == 'rect':
        # Simple rectangle collision
        x1, y1 = entity['rect.top_left'].value
        w, h = entity['rect.size'].value

        x1 += ex
        y1 += ey

        x2 = x1 + w
        y2 = y1 + h

        return x >= x1 and x <= x2 and y >= y1 and y <= y2

    elif gtype == 'circle':
        # Fast circle collision
        cx, cy = entity['circle.center'].value
        r = entity['circle.radius'].value

        cx += ex
        cy += ey

        x -= cx
        y -= cy

        return x ** 2 + y ** 2 <= r ** 2

    elif gtype == 'line':
        # Simple line collision
        x1, y1 = entity['line.start'].value
        x2, y2 = entity['line.end'].value

        x1 += ex
        y1 += ey

        x2 += ex
        y2 += ey

        if x1 > x2:
            x1, x2 = x2, x1
            y1, y2 = y2, y1

        if x2 == x1:
            if abs(x - x1) > 1.0:
                return False

            if y1 > y2:
                y1, y2 = y2, y1

            if y < y1 - 1 or y > y2 + 1:
                return False

            return True

        if x < x1 - 1 or x > x2 + 1:
            return False

        dy = (y2 - y1) / (x2 - x1)
        cy = dy * (x - x1)

        if abs(cy - y) > 1:
            return False

        return True

    else:
        raise ValueError('Unknown geometry type: ' + repr(gtype))


def kill(manager: Manager, ent: Entity, instigator: Entity = None):
    if 'living' in ent:
        h = ent['living']
        h.value = -9999

    ent.add_component('dead')
    del ent['collides']
    
    manager.emit(source, 'death', instigator)

def damage(manager: Manager, ent: Entity, amount: int, instigator: Entity = None):
    if 'living' not in ent:
        return

    h = ent['living']
    h.value -= amount

    if h.value <= 0:
        kill(manager, ent, instigator)


#=== Systems ===

@system_type
class GeometryCollisionSystem(System):
    component_types = ['position', 'collides']

    def on_move(self, source: Entity, dxy: Tuple[int, int], *args, components = None):
        pos = components['position'].value
        within = True

        for e in self.manager:
            if e is not source and 'geometry' in e and 'space' in e:
                if not in_geometry(e, pos):
                    within = False
                    break

        if not within:
            if 'player' in source and 'name' in source:
                self.manager.game.log('{} tried to do what\'s called a noclip move!'.format(get_name(source)))

            pos[0] -= dxy[0]
            pos[0] -= dxy[1]

@system_type
class GeometryPusherSystem(System):
    component_types = ['collides', 'position', 'geometry']

    def on_move(self, source: Entity, dxy: Tuple[int, int], *args, components = None):
        for e in self.manager:
            if e is not source and e.has('collides', 'position') and not e.has_any('immobile', 'geometry'):
                while True:
                    if not in_geometry(source, e['position'].value):
                        pass

                    if not move(e, *dxy):
                        # crushed
                        kill(self.manager, e, source)

                        if e.has('player', 'name'):
                            if 'name' in source:
                                self.manager.game.log('{} was crushed by {}!'.format(get_name(e), get_name(source)))

                            else:
                                self.manager.game.log('{} was crushed by something big!'.format(get_name(e)))

                    elif e.has('player', 'name'):
                        if 'name' in source:
                            self.manager.game.log('{} was pushed by {}!'.format(get_name(e), get_name(source)))

                        else:
                            self.manager.game.log('{} was pushed by something big!'.format(get_name(e)))


@system_type
class GeometryFillRenderSystem(System):
    component_types = ['visible', 'geometry', 'fill']

    def render(self, entity: Entity, pen: CanvasPen, *args, **kwargs):
        gtype = entity['geometry'].value
        fill = entity['fill'].value

        if 'position' in entity:
            ex, ey = entity['position'].value

        else:
            ex = 0
            ey = 0

        if gtype == 'rect':
            # Simple rectangle collision
            x1, y1 = entity['rect.top_left'].value
            w, h = entity['rect.size'].value

            x1 += ex
            y1 += ey

            x2 = x1 + w
            y2 = y1 + h

            pen.move_to((x1, y1))
            pen.rect((w, h), fill)

        elif gtype == 'circle':
            # Fast circle collision
            cx, cy = entity['circle.center'].value
            r = entity['circle.radius'].value

            cx += ex
            cy += ey

            x -= cx
            y -= cy

            for ar_step in range(1, r + 3):
                ar = ar_step / 3

                pen.move_to(cx - ar, cy)
                pen.arc(ar, char = fill)

        elif gtype == 'line':
            # Simple line collision
            x1, y1 = entity['line.start'].value
            x2, y2 = entity['line.end'].value

            x1 += ex
            y1 += ey

            x2 += ex
            y2 += ey

            pen.move_to((x1, y1))
            pen.line_to((x2, y2), fill)

        else:
            raise ValueError('Unknown geometry type: ' + repr(gtype))


@system_type
class LocalRenderSystem(System):
    component_types = ['visible', 'position', 'sprite']

    def overlay(self, entity: Entity, pen: CanvasPen, *args, **kwargs):
        x, y = entity['position'].value
        sprite = entity['sprite'].value
        
        w, h = pen.write_size(sprite)

        pen.move_to((x - w / 2, y - h / 2))
        pen.write(sprite)


@system_type
class GlobalRenderSystem(System):
    component_types = ['visible', 'position', 'overlay']

    def overlay(self, entity: Entity, pen: CanvasPen, *args, **kwargs):
        x, y = entity['position'].value
        sprite = entity['overlay'].value
        
        w, h = pen.write_size(sprite)

        pen.move_to((x - w / 2, y - h / 2))
        pen.write(sprite)


@system_type
class CollisionSystem(System):
    component_types = ['collides', 'position']

    def on_move(self, source: Entity, dxy: Tuple[int, int], *args, components = None):
        pos = components['position'].value

        for e in self.manager:
            if e is not source and 'position' in e:
                pe = e['position'].value

                if pe[0] == pos[0] and pe[1] == pos[1]:
                    if 'collides' in e:
                        if 'player' in source and 'name' in source and 'name' in e:
                            self.manager.game.log('{} bumped with {}!'.format(get_name(source), get_name(e)))

                        pos[0] -= dxy[0]
                        pos[0] -= dxy[1]

                    else:
                        self.manager.emit(source, 'touch', e, dxy)

                    return


@system_type
class PlayerSystem(System):
    component_types = ['player', 'name']

    def on_player_hi(self, source: Entity, **kwargs):
        self.manager.game.log('Hello, {}!'.format(source['name'].value))

    def on_player_up(self, source: Entity, **kwargs):
        assert 'position' in source

        if 'movement' not in source:
            self.manager.game.log('You are immobile!')    

        else:
            self.manager.game.log('You moved up!')
            move(self.manager, source, 0, 1)

    def on_player_down(self, source: Entity, **kwargs):
        assert 'position' in source

        if 'movement' not in source:
            self.manager.game.log('You are immobile!')    

        else:
            self.manager.game.log('You moved down!')
            move(self.manager, source, 0, -1)

    def on_player_left(self, source: Entity, **kwargs):
        assert 'position' in source

        if 'movement' not in source:
            self.manager.game.log('You are immobile!')    

        else:
            self.manager.game.log('You moved left!')
            move(self.manager, source, -1, 0)

    def on_player_right(self, source: Entity, **kwargs):
        assert 'position' in source

        if 'movement' not in source:
            self.manager.game.log('You are immobile!')    

        else:
            self.manager.game.log('You moved right!')
            move(self.manager, source, 1, 0)


def register_systems(manager: Manager):
    for st in sys:
        manager.add_system(st)


# === Entity Building Methods ===

def tree(manager: Manager, x: int, y: int):
    return manager.create_entity([
        ('position', [x, y]),
        ('visible', None),
        ('collides', None),
        ('sprite', '$')
    ])