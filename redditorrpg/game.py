import sqlite3
import typing

from . import players, reddit, init_game
from .entity import Manager, Entity, component_types
from .entitydefs import register_systems
from collections import namedtuple
from .canvas import TextCanvas
from os.path import join as joinpath

try:
    import simplejson as json

except ImportError:
    import json
    


PlayerData = namedtuple('PlayerData', ('id', 'name', 'entity_id'))


class Game(object):
    image_path = 'images'

    def __init__(self, account_filename: str, database_filename: typing.Optional[str] = None, start_pos: typing.Tuple[str, int, int] = ('start', 0, 0)):
        self.manager = Manager(self)
        self.players = [] # type: List[Player]
        self.player_index = {} # type: Dict[str, Player]
        self.start_pos = start_pos

        # Rendering
        self.logs = [] # type: List[str]

        # Database
        self.conn = sqlite3.connect(database_filename or ':memory:')
        self.cursor = self.new_cursor()

        c = self.cursor

        c.execute('CREATE TABLE IF NOT EXISTS Players (' + ', '.join((
            'id STRING',
            'name STRING',
            'entity_id STRING',
        )) + ');')

        c.execute('CREATE TABLE IF NOT EXISTS Components (' + ', '.join((
            'ent_id STRING',
            'name STRING',
            'value STRING', # any JSON-able value,
            'kind STRING',
        )) + ');')

        c.execute('CREATE TABLE IF NOT EXISTS Entities (' + ', '.join((
            'id STRING',
        )) + ');')

        c.execute('CREATE TABLE IF NOT EXISTS Logs (' + ', '.join((
            'line STRING',
        )) + ');')

        register_systems(self.manager)

        for eid, in c.execute('SELECT id FROM Entities;'):
            self.manager.create_entity((), eid)

        for ent_id, name, value, kind in c.execute('SELECT * FROM Components;'):
            self.manager[ent_id].add_component(name, json.loads(str(value)), component_types[kind])

        for _pdata in c.execute('SELECT * FROM Players;'):
            pdata = PlayerData(*_pdata)

            p = players.Player(self, pdata.name, id = pdata.id, entity_id = pdata.entity_id)
            self.players.append(p)
            self.player_index[pdata.name] = p

        c.execute('CREATE TABLE IF NOT EXISTS LastCheck (time REAL);')

        last_check = 0
        
        for check, in c.execute('SELECT time FROM LastCheck'):
            last_check = check
        
        for line, in c.execute('SELECT line FROM Logs'):
            self.logs.append(line)

        if account_filename is not None:
            self.bot = reddit.Bot(self, account_filename, last_check)

        else:
            self.bot = None
        
        self.commit()

        if not self.manager.entities:
            init_game.init_game(self)
            self.save_entities()

    def log(self, line):
        self.logs.append(line)

        if self.bot:
            print('[LOG]', line)

        c = self.cursor
        c.execute('INSERT INTO Logs VALUES (?)', (line,))
        self.commit()

    def play_turn(self):
        if self.bot:
            self.bot.check_submissions()
            
            c = self.cursor
            c.execute('INSERT INTO LastCheck VALUES (?)', (self.bot.last_check,))
            self.commit()
            
        self.save_entities()


    def save_entities(self):
        c = self.cursor

        c.execute('DELETE FROM Entities')
        c.execute('DELETE FROM Components')

        for e in self.manager.entities.values(): # type: Entity
            for cp in e.components:
                c.execute('INSERT INTO Components VALUES (:id, :name, :value, :kind)', {
                    'id': e.id,
                    'name': cp.name,
                    'value': json.dumps(cp.value),
                    'kind': type(cp).__name__
                })

            c.execute('INSERT INTO Entities VALUES (?)', (e.id,))

        self.commit()

    def add_player(self, player: players.Player) -> None:
        self.players.append(player)
        self.player_index[player.name] = player
        print('Added player.')

        c = self.cursor

        c.execute('INSERT INTO Players VALUES (:id, :name, :eid)', {
            'id': player.id,
            'name': player.name,
            'eid': player.entity.id
        })

        self.commit()

    def make_player(self, name: str) -> players.Player:
        player = players.Player(self, name)

        self.add_player(player)
        self.save_entities()

        self.log('Welcome to the game, {}!'.format(name))
        
        return player

    def new_cursor(self) -> sqlite3.Cursor:
        return self.conn.cursor()

    def commit(self) -> None:
        self.conn.commit()

    def player_command(self, command: str, player_name: str):
        if command not in reddit.valid_commands:
            self.log('Invalid commmand {}!   The following commands are available: {}'.format(command, ', '.join(reddit.valid_commands)))
            return

        if player_name not in self.player_index:
            self.make_player(player_name)

        player = self.player_index[player_name]
        player.execute_event(command)

    def render(self, pivot_entity: typing.Optional[Entity]):
        canvas = TextCanvas()

        assert 'position' in pivot_entity
        px, py = pivot_entity['position'].value

        canvas.set_pivot(px, py)
        self.manager.render(canvas)

        res = self.render_logs() + '\n\n\n\n**View:**\n\n\n' + canvas.render(True)
        self.blits = []

        return res

    def render_raw(self, pivot_entity: typing.Optional[Entity]):
        canvas = TextCanvas()

        assert 'position' in pivot_entity
        px, py = pivot_entity['position'].value

        canvas.set_pivot(px, py)
        self.manager.render(canvas)

        res = self.render_logs_raw() + '\n\n    View:\n\n' + canvas.render_raw()
        self.blits = []

        print(repr(res))
        return res

    def render_logs(self):
        res = ['**Logs:**']

        for l in self.logs[-8:]:
            res.append('* ' + l)

        return '\n\n'.join(res)

    def render_logs_raw(self):
        res = ['    Logs:', ''] + ['' for _ in range(8)]

        for i, l in enumerate(self.logs[-8:]):
            res[i + 2] = l

        return '\n'.join(res)

    def tick(self):
        self.manager.tick()

    def update_item(self, player, itype):
        c = self.cursor

        c.execute('DELETE FROM Inventory WHERE id = :id AND itype = :itype', {
            'id': player.id,
            'itype': itype.__name__,
        })

        c.execute('INSERT INTO Inventory VALUES (:id, :itype, :count)', {
            'id': player.id,
            'itype': itype.__name__,
            'count': player.items[itype.__name__]
        })

        self.commit()

    def __del__(self):
        self.conn.close()