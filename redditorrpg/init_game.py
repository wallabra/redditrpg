from .entitydefs import tree
from .mapgen import default_generator


def init_game(game: 'Game'):
    gen = default_generator()
    gen.generate(game.manager)