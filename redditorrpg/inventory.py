import typing
import collections


class MethodSpec(object):
    def __init__(self, name: str, default_value: typing.Any = None):
        self.name = name
        self.default_value = default_value

    def __repr__(self):
        return "[{} {} [{}]]".format(type(self).__name__, self.name, self.default_value)

    def __call__(self, item_type, *args, **kwargs):
        if hasattr(item_type, self.name) and hasattr(getattr(item_type, self.name), '__call__'):
            return getattr(item_type, self.name)(*args, **kwargs)

        elif hasattr(self.default_value, '__call__'):
            return self.default_value(*args, **kwargs)

        else:
            return self.default_value

class ItemType(object):

    def __repr__(self):
        return "ItemType<{}>".format(type(self).__name__)

class ItemTypes(object):
    types = {}

    @staticmethod
    def register(itype):
        ItemTypes.types[itype.__name__] = itype
        return itype

    @staticmethod
    def get_type(iname):
        return ItemTypes.types[iname]

class ItemContainer(object): # aka an inventory!
    def __init__(self):
        self.items = {}
        self.reset_item_updates()

    def reset_item_updates(self):
        self.item_updates = collections.OrderedDict()

    def give_items(self, inv_type: ItemType, count: int) -> None:
        if inv_type in self.items:
            self.items[inv_type.__name__] += count

        else:
            self.items[inv_type.__name__] = count

        self.update_item(inv_type)

    def take_items(self, inv_type: ItemType, count: int) -> int:
        if inv_type in self.items:
            if self.items[inv_type] >= count:
                self.items[inv_type] -= count

                if self.items[inv_type] == 0:
                    del self.items[inv_type]

                return 0 # no debt

            else:
                count -= self.items[inv_type]
                del self.items[inv_type]

                return count # partial debt

        else:
            return count # full debt

        self.update_item(inv_type)

    def update_item(self, inv_type):
        self.item_updates[inv_type.__name__] = self.items.get(inv_type.__name__, 0)

if __name__ == "__main__":
    # Let's say we want to define
    # weapon item types.

    # First, we define that all items can have
    # a method called "mutate_damage", that will,
    # for the sake of example, modify an hypothetical
    # damage value.

    # By default, it returns (0, 1), i.e. no change (1 * a + 0).

    # To define roughly what it should do, we use a
    # MethodSpec instance:
    mutate_damage_m = MethodSpec('mutate_damage', lambda x: x)
    print(mutate_damage_m)

    # Then, for every weapon type, we have to define a set of methods.
    # In our case, every weapon has a mutate_damage method.
    @ItemTypes.register
    class Sword(ItemType):
        @staticmethod
        def mutate_damage(dmg):
            return 1.0 * dmg + 2.0

    @ItemTypes.register
    class Mace(ItemType):
        @staticmethod
        def mutate_damage(dmg):
            return 1.5 * dmg + 1

    # We can also define a helper routine that mutates
    # the damage for us!
    def mutate_damage_for_us(inv_type: typing.Type[ItemType], base_damage: float = 1.0):
        return mutate_damage_m(inv_type, base_damage)

    def print_mutated_damage(inv_type: typing.Type[ItemType], base_damage: float = 1.0):
        print("({}) {} -> {}".format(inv_type.__name__, base_damage, mutate_damage_for_us(inv_type, base_damage)))

    # Now we can print all the damage
    # mutations :)
    print_mutated_damage(Sword, 10)
    print_mutated_damage(Sword, 3)
    print_mutated_damage(Sword, 1)
    print_mutated_damage(Mace, 10)
    print_mutated_damage(Mace, 3)
    print_mutated_damage(Mace, 1)

    # But what if we're holding an item that doesn't
    # have a mutate_damage method, i.e. a non-weapon?
    
    # Like, say... an apple?
    yummy_m = MethodSpec('yummy', False)

    @ItemTypes.register
    class Apple(ItemType):
        @staticmethod
        def yummy():
            return True

        @staticmethod
        def deadly_when_ingested():
            return False

    # Do not worry! The MethodSpec class automatically
    # handles these cases, by checking whether the
    # item type contains the function. In this case,
    # it doesn't, so default value for ya!

    # Or, in our case, since we use a lambda, it is called.
    # This lambda returns our value unchanged, so no mutation :D
    print_mutated_damage(Apple, 10)
    print_mutated_damage(Apple, 1)

    # Hey, at least they're not deadly... and tasty!
    eat_death_m = MethodSpec('deadly_when_ingested', True)

    def eat(item_type):
        print("(eating {})  ".format(item_type.__name__), end='')

        if eat_death_m(item_type):
            print("Gaah! *dies*")

        elif yummy_m(item_type):
            print("Yum!")

        else:
            print("Blargh!")

    @ItemTypes.register
    class Dust(ItemType):
        @staticmethod
        def deadly_when_ingested():
            return False

    eat(Dust) # another one bites the dust
    eat(Sword)
    eat(Apple)