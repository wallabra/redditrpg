from .entitydefs import tree
from .entity import Entity, Manager
from typing import Iterable, Type
from .vector import Vector

import queue
import random



class Door(object):
    def __init__(self, pos: Vector, width: int):
        self.pos = pos
        self.width = width


class BasePrefab(object):
    def __init__(self, manager: Manager, *args, **kwargs):
        self.manager = manager
        self.init_prefab()

    def init_prefab(self, *args, **kwargs):
        pass

    def rect_room(self, x1: int, y1: int, x2: int, y2: int) -> Entity:
        if x2 < x1:
            x1, x2 = x2, x1

        if y2 < y1:
            y1, y2 = y2, y1

        x, y = x1, y1
        w, h = x2 - x1, y2 - y1

        return self.manager.create_entity([
            ('visible',),
            ('geometry', 'rect'),
            ('rect.top_left', [x, y]),
            ('rect.size', [w, h]),
            ('fill', '.'),
            ('space',),
        ])

    def circle_room(self, x: int, y: int, radius: float) -> Entity:
        return self.manager.create_entity([
            ('visible',),
            ('geometry', 'circle'),
            ('circle.center', [x, y]),
            ('circle.radius', [radius]),
            ('fill', '.'),
            ('space',),
        ])

    def doors(self) -> Iterable[Door]:
        pass

    def render(self, pos: Vector) -> Iterable[Entity]:
        pass


class Generator(object):
    def __init__(self):
        self.prefab_types = []
    
    def prefab_type(self, pfclass: Type[BasePrefab]) -> Type[BasePrefab]:
        self.prefab_types.append(pfclass)
        return pfclass

    def generate(self, manager: Manager, start: Vector = (0, 0), max_prefabs = 150):
        start = Vector(start)
        doors = queue.Queue()
        res = []

        p = random.choice(self.prefab_types)(manager)

        res.extend(list(p.render(start)))
        
        for d in p.doors():
            doors.put_nowait(d)

        while max_prefabs > 0 and not doors.empty():
            d = doors.get_nowait()

            possib = []

            for pt in self.prefab_types:
                p = pt(manager)
                pds = []

                for pd in p.doors():
                    if pd.width == d.width:
                        pds.append(pd)

                if pds:
                    possib.append((p, random.choice(pds)))
            
            if possib:
                p, pd = random.choice(possib)
                pos = pd.pos - d.pos

                res.extend(list(p.render(pos)))

                for d in p.doors():
                    doors.put_nowait(pd)

            max_prefabs -= 1

        
def default_generator():
    gen = Generator()

    @gen.prefab_type
    class SimpleRoom(BasePrefab):
        def init_prefab(self):
            self.width = random.randint(5, 15)
            self.height = random.randint(5, 15)

        def doors(self) -> Iterable[Door]:
            if self.width > self.height:
                cy = self.height // 2

                return [
                    Door(Vector((-1, cy)), 1),
                    Door(Vector((self.width + 1, cy)), 1)
                ]

            else:
                cx = self.width // 2

                return [
                    Door(Vector((cx, -1)), 1),
                    Door(Vector((cx, self.height + 1)), 1)
                ]

        def render(self, pos: Vector) -> Iterable[Entity]:
            # main space
            space = self.rect_room(pos.x, pos.y, pos.x + self.width, pos.y + self.height)

            if self.width > self.height:
                cy = self.height // 2

                return [
                    space,

                    # doors
                    self.rect_room(pos.x - 1, cy, pos.x, cy + 1),
                    self.rect_room(pos.x + self.width, cy, pos.x + self.width + 1, cy + 1),
                ]

            else:
                cx = self.width // 2

                return [
                    space,

                    # doors
                    self.rect_room(cx, pos.y - 1, cx + 1, pos.y),
                    self.rect_room(cx, pos.y + self.height, cx + 1, pos.y + self.height + 1),
                ]

    @gen.prefab_type
    class LargeRoom(BasePrefab):
        def init_prefab(self):
            self.width = random.randint(15, 30)
            self.height = random.randint(15, 30)

        def doors(self) -> Iterable[Door]:
            if self.width > self.height:
                cy = self.height // 2

                return [
                    Door(Vector((-1, cy)), 3),
                    Door(Vector((self.width + 1, cy)), 3)
                ]

            else:
                cx = self.width // 2

                return [
                    Door(Vector((cx, -1)), 3),
                    Door(Vector((cx, self.height + 1)), 3)
                ]

        def render(self, pos: Vector) -> Iterable[Entity]:
            # main space
            space = self.rect_room(pos.x, pos.y, pos.x + self.width, pos.y + self.height)

            if self.width > self.height:
                cy = self.height // 2

                return [
                    space,

                    # doors
                    self.rect_room(pos.x - 1, cy, pos.x, cy + 3),
                    self.rect_room(pos.x + self.width, cy, pos.x + self.width + 1, cy + 3),
                ]

            else:
                cx = self.width // 2

                return [
                    space,

                    # doors
                    self.rect_room(cx, pos.y - 1, cx + 3, pos.y),
                    self.rect_room(cx, pos.y + self.height, cx + 3, pos.y + self.height + 1),
                ]

    @gen.prefab_type
    class Garden(LargeRoom):
        def render(self, pos: Vector) -> Iterable[Entity]:
            res = super().render(pos)
            
            for _ in range(5):
                cx = random.randint(0, self.width) + pos.x
                cy = random.randint(0, self.height) + pos.y

                past_pos = []
                ti = random.randint(5, 20)
                
                while len(past_pos) < ti:
                    px = random.randint(cx - 6, cx + 6)
                    py = random.randint(cy - 6, cy + 6)

                    if px < pos.x or px > pos.x + self.width or py < pos.y or py > pos.y + self.height:
                        continue

                    if (px, py) in past_pos:
                        continue

                    past_pos.append((px, py))
                    res.append(tree(self.manager, px, py))

            return res

    return gen
