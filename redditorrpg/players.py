from .entitydefs import InventoryComponent

import random
import uuid
import typing
import math



class Player(object):
    def __init__(self, game, name: str, id: typing.Optional[str] = None, entity_id = None):
        self.game = game
        self.id = id or str(uuid.uuid4())
        self.name = name

        if entity_id is not None:
            self.entity = game.manager[entity_id]

        else:
            pos = self.random_pos()

            self.entity = game.manager.create_entity([
                ('inventory', {}, InventoryComponent),
                ('living', 100,),
                ('position', pos),
                ('sprite', '@'),
                ('visible', None),
                ('collides', None),
                ('movement', None),
                ('player', None),
                ('name', self.name),
            ])

    def random_pos(self):
        possib = []

        for p in self.game.manager:
            if 'geometry' in p:
                gtype = p['geometry'].value

                if 'position' in p:
                    ex, ey = p['position'].value

                else:
                    ex = 0
                    ey = 0

                if gtype == 'rect':
                    x1, y1 = p['rect.top_left'].value
                    w, h = p['rect.size'].value

                    x1 += ex
                    y1 += ey

                    possib.append([
                        random.randint(x1, x1 + w),
                        random.randint(y1, y1 + h),
                    ])

                elif gtype == 'circle':
                    cx, cy = p['circle.center'].value
                    r = p['circle.radius'].value

                    cx += ex
                    cy += ey

                    ang = random.uniform(0, math.pi * 2)
                    dist = random.uniform(0, r)

                    px = math.cos(ang) * dist + cx
                    py = math.si9n(ang) * dist + cy

                    possib.append([
                        int(px),
                        int(py),
                    ])

                else:
                    continue

        return random.choice(possib)

    def update_item(self, inv_type):
        self.game.update_item(self, inv_type)

    def execute_event(self, event, tick=True, *args, **kwargs):
        manag = self.game.manager
        manag.emit(self.entity, ('player', event), *args, **kwargs)

        if tick:
            self.game.tick()