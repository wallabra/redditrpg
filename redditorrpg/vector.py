class Vector(object):
    def __init__(self, vec):
        if isinstance(vec, (list, tuple)):
            self.x = vec[0]
            self.y = vec[1]

        elif hasattr(vec, 'x') and hasattr(vec, 'y'):
            self.x = vec.x
            self.y = vec.y

        else:
            raise ValueError("Non-vectorial value: {}".format(repr(vec)))

    def size(self):    
        l = math.sqrt(float(self.x * self.x) + float(self.y * self.y))
        return l

    def unit(self):
        if self.size() == 0:
            return Vector((0, 0))

        return self / self.size()

    def __add__(self, b):
        return type(self)((self.x + b.x, self.y + b.y))

    def __neg__(self):
        return type(self)((-self.x, -self.y))

    def __mul__(self, b):
        if isinstance(b, type(self)):
            bv = type(self)(b)
            return type(self)((self.x * bv.x, self.y * bv.y))

        else:
            return type(self)((self.x * b, self.y * b))

    def __truediv__(self, b):
        if isinstance(b, type(self)):
            bv = type(self)(b)
            return type(self)((self.x / bv.x, self.y / bv.y))

        else:
            return type(self)((self.x / b, self.y / b))

    def dot(self, b):
        assert isinstance(b, type(self))
        return self.x * b.x + self.y * b.y

    def __sub__(self, b):
        return self + (-b)

    def __repr__(self):
        return "{}(x={},y={})".format(type(self).__name__, self.x, self.y)